## Scenario

2 ESP8266 modules. ("Box A" and "Box B")

There are a few Box As and many Box Bs.

- When pin 5 of the ESP8266 on Box A goes low, Box A notifies all the Box Bs via their IP addresses, the word "Close"
and a key which is sent and compared to the key stored internally on the ESP8266 of Box B.
If the key is True, pin 5 of the ESP8266 on Box B goes high.

- Later when Box A wants to reset all the Box Bs, it notifies all the Box Bs via their IP addresses, the word "Open" 
and the same key. Then pin 5 of the ESP8266 on Box B goes low.

## Installation

### 1. Installing Visual Studio Code. (Skip this if you had already installed on your PC)

- Download and install Visual Studio Code from [here](https://code.visualstudio.com/download)

- Install **PlatformIO IDE** by following [this](http://docs.platformio.org/en/latest/ide/vscode.html#installation) link.

### 2. Download the source code from the gitlab repo.

- Visit https://gitlab.com/rpiguru/esp8266-mqtt

- Download the source code in zip format and extract all files to your hard drive.

### 3. Install the USB driver of the NodeMCU. (Skip this if you had already installed the driver on your PC)

In default, Windows does not support the USB driver of the NodeMCU.

Follow this link - https://cityos-air.readme.io/docs/1-usb-drivers-for-nodemcu-v10#section-13-nodemcu-v10-driver

### 4. Modify firmware of the NodeMCU.

- **Broker**

    * Open the **VS Code**.

    * `File` -> `Open Folder`, and select `MQTT/broker`

    * Open `src/main.cpp` and change some values in the code.
            
            char ssid[] = "";  	                        // your network SSID (name)
            char password[] = "";	                        // your network password
            const char* mqtt_server = "192.168.1.113";                // IP address of the broker.
            char key[] = "first_key";                                 // Key of Box A to be subscribed.

- **Box A(Button)**
    
    * Open the **VS Code**.

    * `File` -> `Open Folder`, and select `MQTT/box_a`

    * Open `src/main.cpp` and change some values in the code.
            
            char ssid[] = "";  	                        // your network SSID (name)
            char password[] = "";	                        // your network password
            const char* mqtt_server = "192.168.1.113";                // IP address of the broker.
            char key[] = "first_key";                                 // Key of Box A to be subscribed.
        
- **Box B (LockModule)**
    
    * Open the **VS Code**.

    * `File` -> `Open Folder`, and select `MQTT/box_b`

    * Open `src/main.cpp` and change some values in the code.
            
            char ssid[] = "";  	                        // your network SSID (name)
            char password[] = "";	                        // your network password
            const char* mqtt_server = "192.168.1.113";                // IP address of the broker.
            char key[] = "first_key";                                 // Key of Box A to be subscribed.
        

- Press `Alt+Ctrl+B` to build the source code.

- Press `Alt+Ctrl+U` to upload to the NodeMCU.

- Press `Alt+Ctrl+S` to open the serial monitor and see how it works.
    
    *NOTE:* NodeMCU's blue LED will blink twice in a second when it tried to connect to the AP thru wifi.
