#include <Arduino.h>
#include <ESP8266WiFi.h>
#include "uMQTTBroker.h"


/*
 * Your WiFi config here
 */
char ssid[] = "";  	                        // your network SSID (name)
char password[] = "";	                        // your network password
const char* mqtt_server = "192.168.1.113";                // IP address of the broker.
char key[] = "first_key";                                 // Key of Box A to be subscribed.

#define     PIN       D1                                  // Pin Number (GPIO5)

unsigned int mqttPort = 1883;
unsigned int max_subscriptions = 30;
unsigned int max_retained_topics = 30;


void data_callback(uint32_t *client, const char* topic, uint32_t topic_len, const char *data, uint32_t lengh) {
  char topic_str[topic_len+1];
  os_memcpy(topic_str, topic, topic_len);
  topic_str[topic_len] = '\0';

  char data_str[lengh+1];
  os_memcpy(data_str, data, lengh);
  data_str[lengh] = '\0';

  Serial.print("received topic '");
  Serial.print(topic_str);
  Serial.print("' with data '");
  Serial.print(data_str);
  Serial.println("'");
}


void setup_wifi() {

  int attempt = 0;
  String macAddr;
  delay(10);
  Serial.print("Connecting to "); Serial.println(ssid);

  WiFi.begin(ssid, password);
  macAddr = WiFi.softAPmacAddress();
  Serial.print("MAC Address: "); Serial.println(macAddr);

  while (WiFi.status() != WL_CONNECTED) {
    // Try to connect for 15 sec, and restart
    if (attempt < 30) attempt ++;
    else ESP.restart();
    // Blink LED while connecting to the router.
    if (attempt % 2 == 0) digitalWrite(D0, HIGH);
    else digitalWrite(D0, LOW);
    Serial.print(".");
    delay(500);
  }
  Serial.println("");
  Serial.print("WiFi connected, IP address: "); Serial.println(WiFi.localIP());
  digitalWrite(D0, HIGH);
}

unsigned long last_change_time = millis();

void on_input_change(){
    if (millis() - last_change_time > 200){
        last_change_time = millis();
        Serial.print("Input is changed - ");
        Serial.println(digitalRead(PIN));
        if (!digitalRead(PIN)){
          Serial.println("Pin went low, closing all Box Bs");
        MQTT_local_publish((unsigned char *)key, (unsigned char *)"Close", 5, 0, 0);
        }
//         else{
//           Serial.println("Pin went high, opening all Box Bs");
// #ifdef IS_BROKER
//         MQTT_local_publish((unsigned char *)key,(unsigned char *)"Open", 4, 0, 0);
// #else
//         client.publish(key, "Open");
// #endif
//         }

    }
}


void setup()
{
  Serial.begin(9600);
  Serial.println();
  Serial.println();
  pinMode(D0, OUTPUT);
  pinMode(PIN, INPUT);
  attachInterrupt(PIN, on_input_change, CHANGE);

  setup_wifi();

  // Register the callback
  MQTT_server_onData(data_callback);
  // Start the broker
  Serial.println("Starting MQTT broker");
  MQTT_server_start(mqttPort, max_subscriptions, max_retained_topics);
  // Subscribe to anything
  MQTT_local_subscribe((unsigned char *)"#", 0);

}

int counter = 0;

void loop()
{
  delay(1000);
}
