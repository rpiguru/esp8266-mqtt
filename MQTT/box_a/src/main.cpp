#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>


/*
 * Your WiFi config here
 */
char ssid[] = "";  	                        // your network SSID (name)
char password[] = "";	                        // your network password
const char* mqtt_server = "192.168.1.113";                // IP address of the broker.
char key[] = "first_key";                                 // Key of Box A to be subscribed.

#define     PIN       D1                                  // Pin Number (GPIO5)

WiFiClient espClient;
PubSubClient client(espClient);
char msg[50];


void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();

  // Switch on the LED if an 1 was received as first character
  if ((char)payload[0] == '1') {
    digitalWrite(BUILTIN_LED, LOW);   // Turn the LED on (Note that LOW is the voltage level
    // but actually the LED is on; this is because
    // it is acive low on the ESP-01)
  } else {
    digitalWrite(BUILTIN_LED, HIGH);  // Turn the LED off by making the voltage HIGH
  }
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Create a random client ID
    String clientId = "ESP8266Client-";
    clientId += String(random(0xffff), HEX);
    // Attempt to connect
    if (client.connect(clientId.c_str())) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      client.publish("outTopic", "hello world");
      // ... and resubscribe
      client.subscribe("inTopic");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void setup_wifi() {

  int attempt = 0;
  String macAddr;
  delay(10);
  Serial.print("Connecting to "); Serial.println(ssid);

  WiFi.begin(ssid, password);
  macAddr = WiFi.softAPmacAddress();
  Serial.print("MAC Address: "); Serial.println(macAddr);

  while (WiFi.status() != WL_CONNECTED) {
    // Try to connect for 15 sec, and restart
    if (attempt < 30) attempt ++;
    else ESP.restart();
    // Blink LED while connecting to the router.
    if (attempt % 2 == 0) digitalWrite(D0, HIGH);
    else digitalWrite(D0, LOW);
    Serial.print(".");
    delay(500);
  }
  Serial.println("");
  Serial.print("WiFi connected, IP address: "); Serial.println(WiFi.localIP());
  digitalWrite(D0, HIGH);
}

unsigned long last_change_time = millis();

void on_input_change(){
    if (millis() - last_change_time > 200){
        last_change_time = millis();
        Serial.print("Input is changed - ");
        Serial.println(digitalRead(PIN));
        if (!digitalRead(PIN)){
          Serial.println("Pin went low, closing all Box Bs");
        client.publish(key, "Close");
        }
//         else{
//           Serial.println("Pin went high, opening all Box Bs");
// #ifdef IS_BROKER
//         MQTT_local_publish((unsigned char *)key,(unsigned char *)"Open", 4, 0, 0);
// #else
//         client.publish(key, "Open");
// #endif
//         }

    }
}


void setup()
{
  Serial.begin(9600);
  Serial.println();
  Serial.println();
  pinMode(D0, OUTPUT);
  pinMode(PIN, INPUT);
  attachInterrupt(PIN, on_input_change, CHANGE);

  setup_wifi();

  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);

}

int counter = 0;

void loop()
{

  if (!client.connected()) {
    reconnect();
  }
  client.loop();

  // snprintf(msg, 75, "hello world #%d", counter++);
  // Serial.print("Publish message: ");
  // Serial.println(msg);
  // client.publish("outTopic", msg);
  delay(1000);
}
