#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>

/*
 * Your WiFi config here
 */
char ssid[] = "";  	                        // your network SSID (name)
char password[] = "";	                        // your network password
const char* mqtt_server = "192.168.1.113";                // IP address of the broker.
char key[] = "first_key";                                 // Key of Box A to be subscribed.

#define     PIN       D1                                  // Pin Number (GPIO5)


WiFiClient espClient;
PubSubClient client(espClient);


void setup_wifi() {

  int attempt = 0;
  String macAddr;
  delay(10);
  Serial.print("Connecting to "); Serial.println(ssid);

  WiFi.begin(ssid, password);
  macAddr = WiFi.softAPmacAddress();
  Serial.print("MAC Address: "); Serial.println(macAddr);

  while (WiFi.status() != WL_CONNECTED) {
    // Try to connect for 15 sec, and restart
    if (attempt < 30) attempt ++;
    else ESP.restart();
    // Blink LED while connecting to the router.
    if (attempt % 2 == 0) digitalWrite(D0, HIGH);
    else digitalWrite(D0, LOW);
    Serial.print(".");
    delay(500);
  }
  Serial.println("");
  Serial.print("WiFi connected, IP address: "); Serial.println(WiFi.localIP());
  digitalWrite(D0, HIGH);
}


void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();

  if (strncmp(key, topic, strlen(topic)) == 0) {
    if (!strncmp((const char*)payload, "Close", 5)) {
      Serial.println("Closing!");
      digitalWrite(PIN, HIGH);
      digitalWrite(D0, HIGH);
    }
    if (!strncmp((const char*)payload, "Open", 4)) {
      Serial.println("Opening!");
      digitalWrite(PIN, LOW);
      digitalWrite(D0, HIGH);
    }
  }
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Create a random client ID
    String clientId = "ESP8266Client-";
    clientId += String(random(0xffff), HEX);
    // Attempt to connect
    if (client.connect(clientId.c_str())) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      // client.publish("outTopic", "hello world");
      // ... and resubscribe
      client.subscribe(key);
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void setup() {
  Serial.begin(9600);

  setup_wifi();

  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
  
  pinMode(D0, OUTPUT);     // Initialize the BUILTIN_LED pin as an output
  pinMode(PIN, OUTPUT);

}

void loop() {

  if (!client.connected()) {
    reconnect();
  }
  client.loop();
  delay(1000);
}
