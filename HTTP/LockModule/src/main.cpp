#include <Arduino.h>
#include <ESP8266WiFi.h>        // Include the Wi-Fi library
#include <ESP8266WebServer.h>
#include <SoftwareSerial.h>
#include <ArduinoOTA.h>

// =================================  Constant Values ==================================
IPAddress staticIP(192, 168, 1, 148); // Commas not dots
// IPAddress staticIP(172, 16, 200, 9); // Commas not dots
IPAddress gateway(192,168,1,1);
// IPAddress gateway(172,16,200,1);
IPAddress subnet(255,255,255,0);
const char* ssid     = "TP-LINK-REAL3D";         // The SSID (name) of the Wi-Fi network you want to connect to
// const char* ssid     = "Solarian1";
const char* password = "dkanehahffk";     // The password of the Wi-Fi network
// const char* password = "preform.cabled.thunder";
String keyCode = "cab1a072";

// =====================================================================================

#define RELAY_PIN   5

ESP8266WebServer server(80);


void unlockCode() {
  String message = "";
  if (server.arg("key") == "") {     // Parameter not found   
    message = "Unspecified Key";
  } 
  else {     // Parameter found
    if (server.arg("key") == keyCode) {
      digitalWrite(RELAY_PIN, LOW);
      message = "Room 15 unlocked";
    } else {
      message = "Room 15 Invalid Key Code";
    }
  }
  Serial.println(message);
  server.send(200, "text/plain", message);
}


void lockCode() { 
  String message = "";
  if (server.arg("key") == "") {
    message = "Unspecified Key";
  } 
  else {
    if(server.arg("key") == keyCode) {
      digitalWrite(RELAY_PIN, HIGH); 
      message = "Room 15 locked!";
    } else {
      message = "Room 15 Invalid Key Code";
    }
  }
  Serial.println(message);
  server.send(200, "text/plain", message);
}


void handleRoot() {
  server.send(200, "text/plain", "Hello world! Lock Here.");   // Send HTTP status 200 (Ok) and send some text to the browser/client
}


void handleNotFound(){
  server.send(404, "text/plain", "404: Not found.."); // Send HTTP status 404 (Not Found) when there's no handler for the URI in the request
}


void setup_wifi() {

  int attempt = 0;
  String macAddr;
  delay(10);
  Serial.print("Connecting to "); Serial.println(ssid);

  WiFi.hostname("Siren1");                  // Must be set BEFORE WiFi.begin
  WiFi.begin(ssid, password);
  macAddr = WiFi.softAPmacAddress();
  Serial.print("MAC Address: "); Serial.println(macAddr);

  while (WiFi.status() != WL_CONNECTED) {
    // Try to connect for 15 sec, and restart
    if (attempt < 30) attempt ++;
    else ESP.restart();
    // Blink LED while connecting to the router.
    if (attempt % 2 == 0) digitalWrite(D0, HIGH);
    else digitalWrite(D0, LOW);
    Serial.print(".");
    delay(500);
  }
  Serial.println("");
  Serial.print("WiFi connected, IP address: "); Serial.println(WiFi.localIP());
  WiFi.config(staticIP, gateway, subnet);
  digitalWrite(D0, HIGH);
}


void setup() {
  Serial.begin(9600);
  delay(10);
  pinMode(RELAY_PIN, OUTPUT);
  pinMode(D0, OUTPUT);
  setup_wifi();
	
  ESP.wdtDisable();
  ESP.wdtEnable(WDTO_8S);
  
  //OTA
  ArduinoOTA.setPassword("swordfish");
  ArduinoOTA.begin();

  server.on("/", handleRoot);               // Call the 'handleRoot' function when a client requests URI "/"
  server.onNotFound(handleNotFound);        // When a client requests an unknown URI (i.e. something other than "/"), call function "handleNotFound"

  server.on("/unlock", unlockCode);         // Associate the handler function to the path 
  server.on("/lock", lockCode);             // Associate the handler function to the path
 
  server.begin();                           // Actually start the server
  Serial.println("HTTP server started");
  
}

void loop() {
  ArduinoOTA.handle(); 
  server.handleClient();                    // Listen for HTTP requests from clients
}
