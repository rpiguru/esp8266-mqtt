#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <LiquidCrystal_I2C.h>
#include <Wire.h>
#include <ArduinoOTA.h>

// int thresholds[16] {210, 157, 92, 19, 394, 361, 330, 282, 516, 494, 469, 442, 599, 583, 566, 546};  // Using CPU as power supply
// int thresholds[16] {217, 163, 105, 25, 396, 364, 328, 282, 514, 494, 469, 439, 597, 581, 565, 543}; // Using separate power supply
int thresholds[16] {374, 289, 181, 32, 609, 573, 531, 480, 729, 710, 684, 658, 800, 786, 773, 758};   // Using separate power supply
char keypad[16] {'1', '2', '3', 'A', '4', '5', '6', 'B', '7', '8', '9', 'C', '*', '0', '#', 'D'};

// WIFI PARAMETERS
IPAddress staticIP(172, 16, 200, 61);   // Commas not dots
IPAddress gateway(172, 16, 200, 1);
IPAddress subnet(255, 255, 255, 0);
// const char* ssid     = "NoSoup4U";         // The SSID (name) of the Wi-Fi network you want to connect to
const char* ssid     = "Solarian1";
// const char* password = "fa3glvzwpd7u976";     // The password of the Wi-Fi network
const char* password = "preform.cabled.thunder";

//String netID =  String("http://192.168.1.");
String netID =  String("http://172.16.200.");
String key = String("?key=cab1a072");         // LOCK/UNLOCK http code
String unLock = "2468";                       // User unlock PIN
String duress = "2469";                       // Duress code
String deLimit = "*";

#define   DURESS_TIME         30000                         // Duress time in milliseconds
#define   startLockNum        9                             // Starting Lock Number
#define   endLockNum          13                            // Ending Lock Number
#define   TIMEOUT             50                            // 50ms for each HTTP request

LiquidCrystal_I2C lcd(0x27, 16, 2);
HTTPClient http;
int response_code = 0;

String k = "";
unsigned long last_duress_time = 0;
unsigned long s_time = 0;
String uKey = "";
bool unlocked = false;


 void setup_wifi() {

  int attempt = 0;
  String macAddr;
  delay(10);
  Serial.print("Connecting to "); Serial.println(ssid);

  WiFi.hostname("Keypad");              // Must be set BEFORE WiFi.begin
  WiFi.begin(ssid, password);
  macAddr = WiFi.softAPmacAddress();
  Serial.print("MAC Address: "); Serial.println(macAddr);

  while (WiFi.status() != WL_CONNECTED) {
    // Try to connect for 15 sec, and restart
    if (attempt < 30) attempt ++;
    else ESP.restart();
    // Blink built-in LED while connecting to the router.
    if (attempt % 2 == 0) digitalWrite(D0, HIGH);
    else digitalWrite(D0, LOW);
    Serial.print(".");
    delay(500);
  }
  WiFi.config(staticIP, gateway, subnet);
  Serial.println("");
  Serial.print("WiFi connected, IP address: "); Serial.println(WiFi.localIP());
  digitalWrite(D0, HIGH);
}


void setup () {
 
  Serial.begin(9600);

  setup_wifi();

  //OTA
  ArduinoOTA.setPassword("swordfish");
  ArduinoOTA.begin();

  Wire.begin(2, 0);

  ESP.wdtDisable();
  ESP.wdtEnable(WDTO_8S);

  http.setTimeout(TIMEOUT);

  // initialize the LCD
  lcd.init();
  lcd.backlight();
  lcd.print("Enter PIN");
}


void unlock(){
  lcd.clear();
  lcd.print("Unlocking");
  if (WiFi.status() == WL_CONNECTED) {                                          // Check WiFi connection status
    for (int lckNum = startLockNum; lckNum < endLockNum; lckNum++) {
      Serial.println(netID + lckNum + "/unlock" + key);      
      http.begin(netID + lckNum + "/unlock" + key);
      s_time = millis();
      response_code = http.GET();                                                // Send the request
      Serial.print("Got response from "); Serial.print(lckNum); Serial.print(" "); 
      Serial.print(response_code); Serial.print(",   Elapsed: ");
      Serial.print(millis() - s_time); Serial.println("ms\nPayload: ");
      if (response_code > 0) {                                                       // Check the returning code
        String payload = http.getString();                                      // Get the request response payload
        Serial.println(payload);                                                // Print the response payload
      }
      http.end();                                                               // Close connection
    }
    lcd.clear();
    lcd.print("Task Complete");
  }
}

void loop() {
  ArduinoOTA.handle();
  
  if (millis() - last_duress_time < DURESS_TIME){
    Serial.println("Still in DURESS TIME!");
    delay(1000);
    return;
  }

  if (!unlocked) {
    for(int i = 0; i < 16; i++)
    {
      if(abs(analogRead(A0) - thresholds[i]) < 5) {
        k = keypad[i];
        if (k == deLimit) {
          if (uKey == duress) {
            Serial.println("Duress is pressed!");
            uKey = ""; 
            lcd.clear();
            lcd.print("***UNLOCKING***"); 
            last_duress_time = millis();
          }
          else if(uKey == unLock) {
            unlock();
            unlocked = true;
          } 
          else {    // Clear the LCD when wrong number is pressed.
            uKey = "";
            lcd.clear();
            //lcd.print("Wrong");
          }
        }
        else {
          uKey += k;
          Serial.println(uKey);
          lcd.print(uKey);
        }

        // Wait until the button is released
        while(analogRead(A0) < 1000) {
          delay(100);
        }
        break;
      }
    }
  }
}
