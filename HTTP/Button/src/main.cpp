#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <ArduinoOTA.h>


// =========================  Constant Values ================================
IPAddress staticIP(172, 16, 200, 60);                    // Commas not dots
IPAddress gateway(172, 16, 200, 1);
IPAddress subnet(255, 255, 255, 0);
const char* ssid     = "Solarian1";
const char* password = "preform.cabled.thunder";
String stringTwo =  String("http://172.16.200.");
String key = String("?key=cab1a072");                       // Added this key to lock the lock
// Lowest IP is for the siren. In this case 9. We are demo'ing 10, 11, 12, 13 as locks 
#define   startLockNum        9                             // Starting Lock Number
#define   endLockNum          13                            // Ending Lock Number
// ============================================================================

#define   BUTTON_PIN          5                             // Use GPIO5
#define   TIMEOUT             50                            // 50ms for each HTTP request

// Global variables
bool message_sent = false;
HTTPClient http;
unsigned long s_time = 0;
unsigned long message_sent_time = 0;
int response_code = 0;
String response = String("");


void setup_wifi() {

  int attempt = 0;
  String macAddr;
  delay(10);
  Serial.print("Connecting to "); Serial.println(ssid);

  WiFi.hostname("Button 1");              // Must be set BEFORE WiFi.begin
  WiFi.begin(ssid, password);
  macAddr = WiFi.softAPmacAddress();
  Serial.print("MAC Address: "); Serial.println(macAddr);

  while (WiFi.status() != WL_CONNECTED) {
    // Try to connect for 15 sec, and restart
    if (attempt < 30) attempt ++;
    else ESP.restart();
    // Blink built-in LED while connecting to the router.
    if (attempt % 2 == 0) digitalWrite(D0, HIGH);
    else digitalWrite(D0, LOW);
    Serial.print(".");
    delay(500);
  }
  WiFi.config(staticIP, gateway, subnet);
  Serial.println("");
  Serial.print("WiFi connected, IP address: "); Serial.println(WiFi.localIP());
  digitalWrite(D0, HIGH);
}


void setup () {

  Serial.begin(9600);
  pinMode(BUTTON_PIN, INPUT);
  pinMode(D0, INPUT);
  setup_wifi();
    
  // OTA
  ArduinoOTA.setPassword("swordfish");
  ArduinoOTA.begin();
  
  ESP.wdtDisable();
  ESP.wdtEnable(WDTO_8S);

  http.setTimeout(TIMEOUT);
}
 
void loop() {

  if (WiFi.status() != WL_CONNECTED) {
    Serial.println("WiFi is disconnected! Reconnecting now...");
    setup_wifi();
  }

  ArduinoOTA.handle();
  if (!digitalRead(BUTTON_PIN)) {
    if (!message_sent){
      Serial.print("\r\nButton Pressed!\r\n");
      for (int lckNum = startLockNum; lckNum < endLockNum; lckNum++) {
        Serial.print("Sending HTTP request - ");
        Serial.println(stringTwo + lckNum + "/lock" + key);      
        http.begin(stringTwo + lckNum + "/lock" + key);
        s_time = millis();
        response_code = http.GET();
        Serial.print("Got response from "); Serial.print(lckNum); Serial.print(" "); 
        Serial.print(response_code); Serial.print(",   Elapsed: ");
        Serial.print(millis() - s_time); Serial.println("ms\nPayload: ");
        if (response_code > 0) {                       // Check the returning code
          response = http.getString();                 // Get the request response payload
          Serial.println(response);                    // Print the response payload 
        }    
        http.end();                                    // Close connection
      }
      message_sent = true;
      message_sent_time = millis();
    }
    else if (millis() - message_sent_time > 30000) {
      // Send message again after 30 sec.
      message_sent = false;
    }
    digitalWrite(D0, HIGH);
  }
  else {
    message_sent = false;
    digitalWrite(D0, LOW);
  }
  
}
